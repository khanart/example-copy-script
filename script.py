import shutil
from pathlib import Path

if __name__ == '__main__':
    input_path = Path.cwd().parent / 'data'
    output_path = input_path.parent / 'output'
    output_path.mkdir(exist_ok=True)
    for file in input_path.iterdir():
        shutil.copy2(file, output_path)
